import os, random
import numpy as np
import pandas as pd
import itertools
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn.metrics import confusion_matrix
import cv2
import matplotlib.pyplot as plt
from matplotlib import ticker
import keras
import tensorflow as tf
from keras import backend as K
from keras.models import Sequential
from keras.layers import Input, Dropout, Flatten, Conv2D, MaxPooling2D, Dense, Activation
from keras.optimizers import RMSprop
from keras.callbacks import ModelCheckpoint, Callback, EarlyStopping
from keras.optimizers import Adam
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers import Input, Dropout, Flatten, Conv2D, MaxPooling2D, Dense, Activation
from keras.optimizers import RMSprop
from keras.callbacks import ModelCheckpoint, Callback, EarlyStopping
from keras.utils import np_utils
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img

############ Main class - MarioBros - Construct, validate and test the model #################
#### Things to do, to improve performance: 
#### - Preprocess data - PCA, Center
#### - Take the images with color

class MarioBros():
	def __init__(self):
		self.clear_cuda_memory()
		config = tf.compat.v1.ConfigProto(device_count = {'GPU':1,'CPU':8})
		config.gpu_options.allow_growth = True
		config.gpu_options.per_process_gpu_memory_fraction=0.9
		sess = tf.compat.v1.Session(config=config)
		tf.compat.v1.keras.backend.set_session(sess)
		print("Importing pre-train model")
		self.vgg16_model = keras.applications.vgg16.VGG16()
		self.vgg16_model.layers.pop()
		self.vgg16_model.layers.pop()
		self.vgg16_model.layers.pop()
		self.vgg16_model.summary()
		self.model = Sequential()
		self.accuracy_model = np.zeros(9)
		self.mse_model = np.zeros(9)
		self.accuracy_neurons = []
		self.loss_neurons = []
		self.accuracy_layer = np.zeros(5)
		self.mse_layer = np.zeros(5)
		self.best_acc_layer = 0.5
		self.best_layer = 2
		self.DATADIR = "/home/pc-casa/Documents/JuanJoseyAde/Proyecto_avanzadisimo/Proyecto/Dataset"
		self.CATEGORIES = ["Nivel 5","Nivel 6"]
		self.CATEGORIES_TEST = ["Nivel 5_test", "Nivel 6_test"]
		self.N = []

	def clear_cuda_memory(self):
		for i in range(5):tf.compat.v1.keras.backend.clear_session()
		return True

	#### Import images from the folders and Build data set - Assign a label to each image 

	def create_training_date(self): 
		self.X_train = []
		self.Y_train = []
		self.X_test = []
		self.Y_test = []
		self.data = []
		self.data_test = []
		for category in self.CATEGORIES:
			path = os.path.join(self.DATADIR, category)
			class_num_0 = self.CATEGORIES.index(category)
			for img in os.listdir(path):
				try:
					img_array = cv2.imread(os.path.join(path,img))
					self.data.append([img_array,class_num_0])
				except Exception as e:
					print("Not working")
					pass
		random.shuffle(self.data)
		for features, label in self.data:
			self.X_train.append(features)
			self.Y_train.append(label)


		for category_test in self.CATEGORIES_TEST:
			path = os.path.join(self.DATADIR, category_test)
			class_num_0 = self.CATEGORIES_TEST.index(category_test)
			for img in os.listdir(path):
				try:
					img_array = cv2.imread(os.path.join(path,img))
					self.data_test.append([img_array,class_num_0])
				except Exception as e:
					print("Not working")
					pass
		random.shuffle(self.data_test)
		for features_test, label_test in self.data_test:
			self.X_test.append(features_test)
			self.Y_test.append(label_test)
		#self.X_0 = np.array(self.X_0).reshape(-1,224, 224,1)

	##### Split the date in: Train data - Validation data - Test data

	def pre_processing(self):

		#self.X = np.array(self.X).reshape(-1,224, 224,1)
		#self.X_train, self.X_test, self.Y_train, self.Y_test = train_test_split(self.X,self.Y,test_size=0.3)
		#self.X_test, self.X_val, self.Y_test, self.Y_val = train_test_split(self.X_test, self.Y_test, test_size=0.5)
		self.X_train = np.asarray(self.X_train)
		self.X_test = np.asarray(self.X_test)
		self.Y_train = np.asarray(self.Y_train)
		self.Y_test = np.asarray(self.Y_test)
		print((self.X_train).shape)
		print((self.X_test).shape)
		print((self.Y_train).shape)
		print((self.Y_test).shape)

		
	def main(self):
		# Import and preprocess the input data
		############################################
		self.create_training_date()
		print("Data imported")
		self.pre_processing()
		print("Data imported and preprocessed")
		############################################
		# One hide layer training in the fully connected network
		############################################
		print("Multiple layers training is about to start")
		#self.oneLayerTraining()
		self.multipleLayers(716)
		print("Multiple layers training is finished")
		############################################
		# Graphs
		############################################
		# One layer Graph
		self.graphMultipleLayers()
		#self.graphOneLayer()

	def graphOneLayer(self):
		# Grafica de error para los distintas funciones de activacion
		plt.plot(self.N, self.accuracy_neurons, '*-')
		plt.title('Precision en la clasificacion para distintas neuronas')
		plt.xlabel('Numero de neuronas (N)')
		plt.ylabel('Precision en la clasificacion')
		plt.grid(b=True)
		plt.show()
		
		# Grafica de error para los distintas funciones de activacion
		plt.plot(self.N, self.loss_neurons, '*-')
		plt.title('Error en la clasificacion para distintas neuronas')
		plt.xlabel('Numero de neuronas (N)')
		plt.ylabel('Error en la clasificacion')
		plt.grid(b=True)
		plt.show()

	def graphMultipleLayers(self):
		# Grafica de error para los distintas funciones de activacion
		plt.plot(self.L, self.accuracy_layer, '*-')
		plt.title('Precision en la clasificacion para distintas capas ocultas')
		plt.xlabel('Numero de capas ocultas (L)')
		plt.ylabel('Precision en la clasificacion')
		plt.grid(b=True)
		plt.show()
		
		# Grafica de error para los distintas funciones de activacion
		plt.plot(self.L, self.mse_layer, '*-')
		plt.title('Error en la clasificacion para distintas capas ocultas')
		plt.xlabel('Numero de capas ocultas (L)')
		plt.ylabel('Error en la clasificacion')
		plt.grid(b=True)
		plt.show()
	
	def oneLayerTraining(self):
		self.N = [2,716,1430,2144,2858,3572]  ##[0,1,2,3,4,5]
		epochs = [8,6,4,2]
		best_acc = 0.5
		best_neurons_model = 2
		i = 0
		for i in range(0,6):
			print("Creating new model")
			self.model = Sequential()
			for layer in self.vgg16_model.layers:
				self.model.add(layer)
			for layer in self.model.layers: #Since the model is already trained with certain weights, we dont want to change it. Let it be the same
				layer.trainable = False
			self.model.add(Dense(int(self.N[i]), activation="sigmoid"))
			self.model.add(Dense(1, activation= 'linear'))
			self.model.compile(loss = 'mean_squared_error',optimizer='adam',metrics=['accuracy'])
			self.model.summary()
			self.model.fit(self.X_train, self.Y_train, batch_size = 1,epochs=2, verbose= 1)
			self.model.save('oneLayerModel_'+str(self.N[i])+'_epoch_'+str(2))
			mse, test_acc = self.model.evaluate(self.X_test, self.Y_test, verbose = 1)
			self.accuracy_neurons.append(test_acc)
			self.loss_neurons.append(mse)
			self.accuracy_model[i] = test_acc
			self.mse_model[i] = mse
			if self.accuracy_model[i] > best_acc:
				best_acc = self.accuracy_model[i]
				best_neurons_model = self.N[i]
			i = i+1
			print(i)
		print("Numero de neuronas con mejor exactitud: "+str(best_neurons_model))
		print("Exactitud: "+str(best_acc))
		#multipleLayers(best_neurons_model1)

	def multipleLayers(self, neuronas):
		self.L = [2,3,4,5,6]
		self.accuracy_layer = np.zeros(5)
		self.mse_layer = np.zeros(5)
		self.best_acc_layer = 0.5
		self.best_layer = 2
		i = 0
		for i in range(0,5):
			print("Creating new model")
			self.model = Sequential()
			for layer in self.vgg16_model.layers:
				self.model.add(layer)
			for layer in self.model.layers: #Since the model is already trained with certain weights, we dont want to change it. Let it be the same
				layer.trainable = False
			for j in range(0,int(self.L[i])):
				self.model.add(Dense(neuronas, activation="sigmoid"))
			self.model.add(Dense(1, activation= 'linear'))
			self.model.compile(loss = 'mean_squared_error',optimizer='adam',metrics=['accuracy'])
			self.model.summary()
			self.model.fit(self.X_train, self.Y_train, batch_size = 1,epochs=10, verbose= 1)
			self.model.save('multipleLayerModel_Last'+str(int(self.L[i]))+'_epoch_'+str(10))
			mse, test_acc = self.model.evaluate(self.X_test, self.Y_test, verbose = 1)
			self.accuracy_layer[i] = test_acc
			self.mse_layer[i] = mse
			if self.accuracy_layer[i] > self.best_acc_layer:
				self.best_acc_layer= self.accuracy_layer[i]
				self.best_layer = int(self.L[i])



if __name__=='__main__':
	marioBros = MarioBros()
	marioBros.main()
