from nes_py.wrappers import JoypadSpace
import numpy as np
import cv2
import gym_super_mario_bros
import keyboard
from pynput import keyboard
import threading
from gym_super_mario_bros.actions import COMPLEX_MOVEMENT
import time

########################### Clase MarioTeleop ###########################################################################
## Clase encargada de:
## - Adquirir los eventos de las teclas para poder jugar en el ambiente de GYM de Super Mario Bros
## - Guardar las imagenes con cierta frecuencia - Cada 20 frames del juego

class MarioTeleop:
	def __init__(self):
		self.nivel = 0
		self.stage = 0
		self.current_keys = set()
		self.combination_to_function = {
		frozenset([keyboard.KeyCode(char ='a'), keyboard.KeyCode(char ='p')]): self.action_7,
		frozenset([keyboard.KeyCode(char ='a'), keyboard.KeyCode(char ='o')]): self.action_8,
		frozenset([keyboard.KeyCode(char ='a'), keyboard.KeyCode(char ='p'), keyboard.KeyCode(char ='o')]): self.action_9,
		frozenset([keyboard.KeyCode(char ='d'), keyboard.KeyCode(char ='p')]): self.action_2,
		frozenset([keyboard.KeyCode(char ='d'), keyboard.KeyCode(char ='o')]): self.action_3,
		frozenset([keyboard.KeyCode(char ='d'), keyboard.KeyCode(char ='o'), keyboard.KeyCode(char ='p')]): self.action_4
		}
		self.COMPLEX_MOVEMENT = [
			['NOOP'],
			['right'],
			['right', 'A'],
			['right', 'B'],
			['right', 'A', 'B'],
			['A'],
			['left'],
			['left', 'A'],
			['left', 'B'],
			['left', 'A', 'B'],
			['down'],
			['up'],
		]
		self.action = 0

	def action_2(self):
		self.action = 2

	def action_3(self):
		self.action = 3

	def action_4(self):
		self.action = 4

	def action_7(self):
		self.action = 7

	def action_8(self):
		self.action = 8

	def action_9(self):
		self.action = 9

	def on_press(self, key):
		#print('{0} pressed'.format(key))
		self.current_keys.add(key)
		print(self.current_keys)
		if frozenset(self.current_keys) in self.combination_to_function:
			self.combination_to_function[frozenset(self.current_keys)]()
		
		elif key.char == 'a':   #left
			self.action = 6
		elif key.char == 'd': #right
			self.action = 1
		elif key.char == 's': #down
			self.action = 10 
		elif key.char == 'p':  #jump
			self.action = 5
		elif key.char == 'w':
			self.action = 11
		else:
			self.action = 0  

	def on_release(self, key): #Funcion al soltar una tecla
		print('{0} release'.format(key))
		self.action = 0
		if key == keyboard.Key.esc: #Con ESC finaliza este thread
			return False
		self.current_keys.remove(key)

	def ThreadInputs(self): #Listener de Pynput en otro thread
		with keyboard.Listener(on_press=self.on_press, on_release=self.on_release) as listener:
			listener.join()


	def main(self):
		done = True
		i = 0
		self.nivel = input('Ingrese el nivel en que desea guardar las imagenes ')
		self.stage = input('Ingrese la etapa en que desea guardar las imagenes ')
		env = gym_super_mario_bros.make('SuperMarioBros-'+str(self.nivel)+'-'+str(self.stage)+'-v0')
		env = JoypadSpace(env, COMPLEX_MOVEMENT)
		threading.Thread(target=self.ThreadInputs).start()
		print('Guardando images del nivel: '+str(self.nivel)+' - Etapa: '+str(self.stage)+'....')
		while True:
			if done:
				state = env.reset()
			state, reward, done, info = env.step(self.action)
			if i%20 ==0 :
				new_image = cv2.resize(state,(224,224))
				cv2.imwrite('Dataset/Nivel '+str(self.nivel)+'/Stage '+str(self.stage)+'/Image_'+str(i)+'_lvl_'+str(self.nivel)+'_Stage_'+str(self.stage)+'.png', cv2.cvtColor(new_image, cv2.COLOR_RGB2BGR))
			env.render()
			time.sleep(0.01)
			i+=1
			if i == 60000:
				break
		env.close()

if __name__ == '__main__':
	marioTeleop = MarioTeleop()
	marioTeleop.main()