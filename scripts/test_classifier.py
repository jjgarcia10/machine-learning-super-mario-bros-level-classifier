import os, random
import numpy as np
import pandas as pd
import itertools
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn.metrics import confusion_matrix
import cv2
import matplotlib.pyplot as plt
from matplotlib import ticker
#import keras
import tensorflow as tf
from keras import backend as K
from keras.models import Sequential
from keras.layers import Input, Dropout, Flatten, Conv2D, MaxPooling2D, Dense, Activation
from keras.optimizers import RMSprop
from keras.callbacks import ModelCheckpoint, Callback, EarlyStopping
from keras.optimizers import Adam
from keras.utils import np_utils
from keras.models import Sequential, load_model
from keras.layers import Input, Dropout, Flatten, Conv2D, MaxPooling2D, Dense, Activation
from keras.optimizers import RMSprop
from keras.callbacks import ModelCheckpoint, Callback, EarlyStopping
from keras.utils import np_utils
from keras.applications import vgg16
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img

class TestMarioBros():

	def __init__(self):
		self.DATADIR = "/home/juanjosegarcia/Documents/Machine Learning/Proyecto/Dataset"
		self.CATEGORIES_TEST_EASY = ["Nivel 1_val", "Nivel 8_val"]
		self.CATEGORIES_TEST_HARD = ["Nivel 5_val", "Nivel 6_val"]
		print("Importing pre-train model")
		self.vgg16_model = vgg16.VGG16()
		self.vgg16_model.layers.pop()
		self.vgg16_model.layers.pop()
		self.vgg16_model.layers.pop()
		self.vgg16_model.summary()
		self.model = Sequential()

	def create_validation_data(self): 
		self.X_val_easy = []
		self.Y_val_easy = []
		self.X_val_hard = []
		self.Y_val_hard = []
		self.data_easy = []
		self.data_hard = []

		############# Easy model - Level 1 and Level 8 ###################

		for category_easy in self.CATEGORIES_TEST_EASY:
			path = os.path.join(self.DATADIR, category_easy)
			class_num_0 = self.CATEGORIES_TEST_EASY.index(category_easy)
			for img in os.listdir(path):
				try:
					img_array = cv2.imread(os.path.join(path,img))
					self.data_easy.append([img_array,class_num_0])
				except Exception as e:
					print("Not working")
					pass
		random.shuffle(self.data_easy)
		for features_easy, label_easy in self.data_easy:
			self.X_val_easy.append(features_easy)
			self.Y_val_easy.append(label_easy)


		############# Hard model - Level 5 and Level 6 ###################

		for category_hard in self.CATEGORIES_TEST_HARD:
			path = os.path.join(self.DATADIR, category_hard)
			class_num_0 = self.CATEGORIES_TEST_HARD.index(category_hard)
			for img in os.listdir(path):
				try:
					img_array = cv2.imread(os.path.join(path,img))
					self.data_hard.append([img_array,class_num_0])
				except Exception as e:
					print("Not working")
					pass
		random.shuffle(self.data_hard)
		for features_hard, label_hard in self.data_hard:
			self.X_val_hard.append(features_hard)
			self.Y_val_hard.append(label_hard)


	def pre_processing(self):

		self.X_val_easy = np.asarray(self.X_val_easy)
		self.X_val_hard = np.asarray(self.X_val_hard)
		self.Y_val_easy = np.asarray(self.Y_val_easy)
		self.Y_val_hard = np.asarray(self.Y_val_hard)
		print((self.X_val_easy).shape)
		print((self.X_val_hard).shape)
		print((self.Y_val_easy).shape)
		print((self.Y_val_hard).shape)

	def test_easy_models(self):
		self.create_base_model()
		self.model.load_weights('/home/juanjosegarcia/Documents/Machine Learning/Proyecto/Easy_Model/oneLayerModelo_epochs10/oneLayerModel_716_epoch_10')
		mse, test_acc = self.model.evaluate(self.X_val_easy, self.Y_val_easy, verbose = 1)
		print('---------- Best Easy Model ------------')
		print('----- Error:'+str(mse))
		print('----- Accuracy'+str(test_acc))

	def create_base_model(self):
		self.model = Sequential()
		for layer in self.vgg16_model.layers:
			self.model.add(layer)
		for layer in self.model.layers: #Since the model is already trained with certain weights, we dont want to change it. Let it be the same
			layer.trainable = False
		self.model.add(Dense(716, activation="sigmoid"))
		self.model.add(Dense(716, activation="sigmoid"))
		self.model.add(Dense(716, activation="sigmoid"))
		self.model.add(Dense(716, activation="sigmoid"))
		self.model.add(Dense(1, activation= 'linear'))
		self.model.compile(loss = 'mean_squared_error',optimizer='adam',metrics=['accuracy'])


	def test_hard_models(self):
		self.create_base_model()
		#del self.model
		self.model.load_weights('/home/juanjosegarcia/Documents/Machine Learning/Proyecto/Hard_Model/multiLayerModels/multipleLayerModel_Last4_epoch_10')
		mse, test_acc = self.model.evaluate(self.X_val_hard, self.Y_val_hard, verbose = 1)
		print('---------- Best Hard Model ------------')
		print('----- Error:'+str(mse))
		print('----- Accuracy'+str(test_acc))


	def main(self):
		# Import and preprocess the input data
		############################################
		self.create_validation_data()
		print("Data imported")
		self.pre_processing()
		print("Data imported and preprocessed")
		###########################################
		# Test Easy model
		#self.test_easy_models()
		###########################################
		# Test Hard model
		self.test_hard_models()



if __name__=='__main__':
	marioBros = TestMarioBros()
	marioBros.main()
